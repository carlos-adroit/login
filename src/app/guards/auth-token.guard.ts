import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiServiceService } from '../services/api-service.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthTokenGuard implements CanActivate {

  constructor(
    private apiService: ApiServiceService,
    private router: Router
  ) {}

  canActivate(): Observable<boolean> | boolean {
    if (localStorage.getItem('id')) {
      return true;
    } else {
      this.router.navigateByUrl('/auth');
      return false;
    }
  }
}
