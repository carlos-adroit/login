import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from "sweetalert2";

import { ApiServiceService } from '../../../services/api-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private apiService: ApiServiceService
    ) { }


  userForm: FormGroup = this.fb.group({
    email: ['', [Validators.required]],
    password: ['', [Validators.required]]
  });


  login() {
    const {email, password} = this.userForm.value;

    this.apiService.login(email, password)
      .subscribe(data => {
        if (data.id) {
          this.router.navigateByUrl('/dashboard');

          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            icon: 'success',
            title: 'Login Successfully',
            showConfirmButton: false,
            timerProgressBar: true,
            timer: 1500,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer),
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          });

          Toast.fire({
            icon: 'success',
            title: 'Login successfully'
          });

        } else {
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            icon: 'error',
            title: 'Login fail',
            showConfirmButton: false,
            timerProgressBar: true,
            timer: 3000,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer),
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          });

          Toast.fire({
            icon: 'error',
            title: 'Login Failed'
          });
        }
      });
  }
}
