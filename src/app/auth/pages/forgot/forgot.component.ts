import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent {

  myForm: FormGroup = this.fb.group({
    email: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {}

  sendEmail() {
    console.log(this.myForm.value);

    this.router.navigateByUrl('/dashboard');
  }

}
