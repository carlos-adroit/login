export interface AuthUser {
  email: string,
  password: string
}

export interface AuthResponse {
  id:      string;
  ttl:     number;
  created: Date;
  userId:  string;
  user:    User;
}

export interface User {
  name:                string;
  role:                string;
  fmoAccess:           string;
  cashRegisterType:    string;
  isTesting:           boolean;
  isDeveloper:         boolean;
  birthday:            Date;
  branchIds:           string[];
  shortCuts:           any[];
  personalInfo:        PersonalInfo;
  contractPermissions: ContractPermissions;
  isActive:            boolean;
  isCompanyAdmin:      boolean;
  plantId:             null;
  hasCard:             boolean;
  isFarmPurchases:     boolean;
  accessPosition:      boolean;
  passwordStatus:      string;
  phone:               string;
  isAuditor:           boolean;
  username:            string;
  email:               string;
  id:                  string;
  isPlantManager:      boolean;
  isBillsAdmin:        string;
  showPositionPrices:  string;
  printPositionChart:  string;
  farmPaymentsAccess:  boolean;
  winnipegSettlment:   boolean;
  canAuthorizeInvoice: string;
  enabledEditTickets:  boolean;
  removePositionChart: boolean;
  isOwndeals:          string;
  iosAppLink:          null;
  category:            string;
}

export interface ContractPermissions {
  create: boolean;
  read:   boolean;
  update: boolean;
  delete: boolean;
}

export interface PersonalInfo {
  department:        string;
  location:          string;
  employeeNumber:    string;
  addressStreetLine: string;
  city:              string;
  postalCode:        string;
  province:          string;
  country:           string;
  dateofJoining:     string;
  positionTitle:     string;
  employeeType:      string;
}

export interface Origin {
  branchId:    string;
  createdById: string;
  createdAt:   Date;
  name:        string;
  id?:          string;
  createdBy?:  CreatedBy;
}

export interface CreatedBy {
  name:                string;
  role:                string;
  fmoAccess:           string;
  cashRegisterType:    string;
  isTesting:           boolean;
  isDeveloper:         boolean;
  birthday:            Date;
  branchIds:           string[];
  shortCuts:           any[];
  personalInfo:        PersonalInfo;
  contractPermissions: ContractPermissions;
  isActive:            boolean;
  isCompanyAdmin:      boolean;
  plantId:             null;
  hasCard:             boolean;
  isFarmPurchases:     boolean;
  accessPosition:      boolean;
  passwordStatus:      string;
  phone:               string;
  isAuditor:           boolean;
  username:            string;
  email:               string;
  id:                  string;
  isPlantManager:      boolean;
  farmPaymentsAccess:  boolean;
  winnipegSettlment:   boolean;
  canAuthorizeInvoice: string;
  enabledEditTickets:  boolean;
  removePositionChart: boolean;
  isOwndeals:          string;
  iosAppLink:          null;
  category:            null | string;
  canApproveClients?:  boolean;
  isWarehouse?:        string;
  lastConection:       Date;
  desktopVersion:      string;
  prevBranch:          string;
  homeViewed:          boolean;
  mobileVersion?:      string;
}

export interface Branches {
  name:     string;
  nickname: string;
  color:    string;
  id:       string;
}
