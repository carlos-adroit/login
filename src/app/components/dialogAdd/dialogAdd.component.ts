import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';

import { ApiServiceService } from '../../services/api-service.service';
import { Branches } from '../../auth/interfaces/interface';
import { ListComponent } from '../list/list.component';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialogAdd',
  templateUrl: './dialogAdd.component.html',
  styleUrls: ['./dialogAdd.component.css']
})
export class DialogAddComponent implements OnInit{

  originForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    branchId: ['', Validators.required],
    createdById: [localStorage.getItem('idUser'), Validators.required]
  });

  branchesArray: Branches[] = [];

  @ViewChild(ListComponent) list!: ListComponent;

  constructor(
    public dialogRef: MatDialogRef<DialogAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private apiService: ApiServiceService,
    private fb: FormBuilder
  ) {}


  ngOnInit(): void {
    this.getBranches();
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  createOrigin() {
    this.apiService.createOrigin(this.originForm.value)
      .subscribe( data => {
        Swal.fire({
          icon: 'success',
          title: `Your origin ${data.name} has been created`,
          showConfirmButton: false,
          timer: 1500
        });

        this.getOrigin();

        this.originForm.controls['name'].setValue('');
        this.originForm.controls['branchId'].setValue('');
      });

    this.dialogRef.close();
  }

  getOrigin() {
    this.apiService.getOrigin()
      .subscribe(data => {
        this.apiService.origin = data;
      })
  }

  getBranches() {
    this.apiService.getBranches()
      .subscribe(data => {
        this.branchesArray = data;
      });
  }


}
