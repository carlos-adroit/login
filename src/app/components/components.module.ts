import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MaterialModule } from '../material/material.module';
import { ListComponent} from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogAddComponent } from './dialogAdd/dialogAdd.component';
import { DialogEditComponent } from './dialogEdit/dialogEdit.component';


@NgModule({
  declarations: [
    DashboardComponent,
    ListComponent,
    DialogAddComponent,
    DialogEditComponent
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ComponentsModule { }
