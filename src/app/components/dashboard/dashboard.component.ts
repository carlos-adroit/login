import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import Swal from "sweetalert2";

import { ApiServiceService } from '../../services/api-service.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{

  name: string = localStorage.getItem('nameUser') || '';

  constructor(
    private router: Router,
    private apiService: ApiServiceService,
    private fb: FormBuilder
    ) {}

  ngOnInit(): void {
    // this.getOrigin();
  }

  getOrigin() {
    this.apiService.getOrigin()
      .subscribe(data => {
        this.apiService.origin = data;
        console.log(this.apiService.origin);
      });
  }

  get user() {
    return this.apiService.user;
  }

  logout() {
    this.router.navigateByUrl('/auth/login');
    this.apiService.logout();

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      icon: 'success',
      title: 'Logout Successfully',
      showConfirmButton: false,
      timerProgressBar: true,
      timer: 3000,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer),
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: 'Logout successfully'
    })

  }


}
