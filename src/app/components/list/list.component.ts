import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import Swal from "sweetalert2";

import { ApiServiceService } from '../../services/api-service.service';
import { Origin } from '../../auth/interfaces/interface';
import { DialogAddComponent } from '../dialogAdd/dialogAdd.component';
import { DialogEditComponent } from '../dialogEdit/dialogEdit.component';
import { debounceTime } from 'rxjs';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit{

  // example variables
  animal: string = '';
  name: string = '';

  // origin: Origin[] = this.apiService.origin;
  get origin() {
    return this.apiService.origin;
  }

  nameOrigin: string = '';
  searchValue: {} = {};

  originForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    branchId: ['', Validators.required],
    createdById: [localStorage.getItem('userId')]
  });


  constructor(
    private apiService: ApiServiceService,
    public dialog: MatDialog,
    private fb: FormBuilder
  ) {}


  ngOnInit(): void {
    this.getOrigin();
  }


  getOrigin() {
    this.apiService.getOrigin()
      .subscribe(data => {
        this.apiService.origin = data;
      })
  }


  search() {
    if (this.nameOrigin) {

      this.searchValue = {
        where: {
          name: {like: this.nameOrigin}
        },
        include: [
          'branch',
          'createdBy'
        ]
      }

      this.apiService.getOriginSearch(this.searchValue)
        .pipe(
          debounceTime(500)
        )
        .subscribe((data: any) => {
          this.apiService.origin = data;
        });

    } else if(this.nameOrigin == '') {
      this.ngOnInit();
    }
  }

  displayedColumns: string[] = ['position', 'name', 'nickname', 'created', 'operations'];

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAddComponent, {
      data: {name: this.name, animal: this.animal},
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogEdit(id: string): void {
    const dialogRef = this.dialog.open(DialogEditComponent, {
      data: id,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  deleteOrigin(id: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#161350',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.apiService.deleteOrigin(id)
          .subscribe(data => {
            this.getOrigin();
          });

        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  }
}
