import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { ApiServiceService } from '../../services/api-service.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Branches, Origin } from 'src/app/auth/interfaces/interface';
import Swal from 'sweetalert2';

interface Food {
  value: string;
  viewValue: string;
}

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-dialogEdit',
  templateUrl: './dialogEdit.component.html',
  styleUrls: ['./dialogEdit.component.css']
})
export class DialogEditComponent implements OnInit {

  idOrigin: string = '';
  branches: Branches[] = [];
  originUpdatedForm: FormGroup = this.fb.group({
    name: [''],
    branchId: ['']
  });
  origin: any = {};

  ngOnInit() {
    this.getBranches();
    this.getOriginId();
  }

  constructor(
    public dialogRef: MatDialogRef<DialogEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private apiService: ApiServiceService,
    private fb: FormBuilder
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  getOrigin() {
    this.apiService.getOrigin()
      .subscribe(data => {
        this.apiService.origin = data;
      })
  }

  updateOrigin() {
    // console.log(this.originUpdatedForm.value);
    this.apiService.updateOrigin(this.idOrigin, this.originUpdatedForm.value)
      .subscribe(data => {
        Swal.fire({
          icon: 'success',
          title: 'Your origin has been updated',
          showConfirmButton: false,
          timer: 1500
        });

        this.getOrigin();
      });
  }

  getOriginId() {
    this.apiService.getOriginId(this.data)
      .subscribe(data => {
        this.origin = data;
        this.idOrigin = this.origin.id;

        this.originUpdatedForm.controls['name'].setValue(this.origin.name);
        this.originUpdatedForm.controls['branchId'].setValue(this.origin.branch.id);
      })
  }

  getBranches() {
    this.apiService.getBranches()
      .subscribe((data) => {
        this.branches= data;
      });
  }

}
