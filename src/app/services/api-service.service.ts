import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { of } from 'rxjs';
import { catchError, map, tap } from "rxjs/operators";

import { AuthUser, AuthResponse, User, Origin, Branches } from '../auth/interfaces/interface';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private API_URL: string = environment.api_url;
  private _user!: AuthResponse;
  origin: Origin[] = [];
  filterOriginBranch = {
    where: { "branchId": "5defe517dcf0af574296a00e"},
    include: [
      'branch',
      'createdBy'
    ]
  }

  filterOrigin = {
    include: [
      'branch',
      'createdBy'
    ]
  }


  constructor(
    private http: HttpClient
  ) { }


  get user() {
    return {...this._user}
  }


  login(email: string, password: string) {
    const url = `${this.API_URL}/AppUsers/login?include=user`;
    const body = {email, password};

    return this.http.post<AuthResponse>(url, body)
      .pipe(
        tap(data => {
          if (data.id) {
            localStorage.setItem("id", data.id);
            localStorage.setItem("idUser", data.user.id);
            localStorage.setItem("nameUser", data.user.name);

            this._user = {
              id:      data.id,
              ttl:     data.ttl,
              created: data.created,
              userId:  data.userId,
              user:    data.user
            }
          }
        }),
        map(data => data),
        catchError(err => of(err.error.error))
      );
  }


  getOrigin() {
    const id = localStorage.getItem("id");
    const url = `${this.API_URL}/Origins?filter=${JSON.stringify(this.filterOriginBranch)}&access_token=${id}`;
    return this.http.get<Origin[]>(url);
  }


  getOriginId(id: any) {
    const token = localStorage.getItem('id');
    const url = `${this.API_URL}/Origins/${id}?filter=${JSON.stringify(this.filterOrigin)}&access_token=${token}`;

    return this.http.get(url);
  }


  getOriginSearch(filterSearch: {}) {
    const token = localStorage.getItem('id');
    const url = `${this.API_URL}/Origins?filter=${JSON.stringify(filterSearch)}&access_token=${token}`;

    return this.http.get(url);
  }

  getBranches() {
    const id = localStorage.getItem("id")
    const url = `${this.API_URL}/Branches?access_token=${id}`;

    return this.http.get<Branches[]>(url);
  }


  createOrigin(origin: Origin) {
    const token = localStorage.getItem('id');
    const url = `${this.API_URL}/Origins?access_token=${token}`;

    return this.http.post<Origin>(url, origin);
  }


  updateOrigin(id: string, origin: Origin) {
    const token = localStorage.getItem('id');
    const url = `${this.API_URL}/Origins/${id}?access_token=${token}`;

    return this.http.patch(url, origin);
  }


  deleteOrigin(id: string) {
    const token = localStorage.getItem('id');
    const url = `${this.API_URL}/Origins/${id}?access_token=${token}`;

    return this.http.delete(url);
  }


  logout() {
    localStorage.clear();
  }


}
